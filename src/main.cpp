#include <iostream>
#include <fstream>
#include <iterator>
#include <cstdint>
#include <vector>
#include <memory>
#include <iomanip>
#include <limits>
#include <random>

#include "tensor/tensor.hpp"
#include "utils.hpp"

decltype(auto) read_picture(std::ifstream& images,
                            uint32_t rows, uint32_t cols)
{
  tensor<float, 3> pic(1, rows, cols);

  using value_type = float/*typename tensor<float, 3>::value_type*/;

  auto convert = [](auto a) -> value_type {
    return static_cast<value_type>(a) / std::numeric_limits<value_type>::max();
  };

  for (std::size_t i = 0; i < rows; ++i) {
    for (std::size_t j = 0; j < cols; ++j) {
      auto px = read_from_stream<uint8_t>(images);
      *pic.ptr(0, i, j) = convert(px);
    }
  }

  return pic;
}

template<typename StreamT>
void dump_letter(StreamT& stream, tensor<float, 3>& pic)
{
  for (size_t i = 0; i < 3/*pic.dim<H>()*/; ++i) {
    for (size_t j = 0; j < 4/*pic.dim<W>()*/; ++j) {
      auto v = pic.value(0, i, j);
      stream << std::hex;
      stream << std::setfill('0');
      stream << std::setw(2);
      stream << static_cast<uint32_t>(v * std::numeric_limits<decltype(v)>::max());
      stream << "|";
    }
    stream << std::endl;
  }
}

decltype(auto) compute_convolution(size_t kernels,
                                   size_t kernel_height,
                                   size_t kernel_width,
                                   size_t channels,
                                   size_t input_height,
                                   size_t input_width,
                                   size_t output_height,
                                   size_t output_width,
                                   tensor<float, 4>& weights,
                                   tensor<float, 3>& input)
{
  tensor<float, 3> output_tensor{kernels, output_height, output_width};
  
  for (size_t k = 0; k < kernels; ++k) {
    for (size_t oh = 0; oh < output_height; ++oh) {
      for (size_t ow = 0; ow < output_width; ++ow) {
        for (size_t c = 0; c < channels; ++c) {
          for (size_t ih = 0; ih < input_height; ++ih) {
            for (size_t iw = 0; iw < input_width; ++iw) {
              for (size_t kh = 0; kh < kernel_height; ++kh) {
                for (size_t kw = 0; kw < kernel_width; ++kw) {
                  *output_tensor.ptr(k, oh, ow) +=
                    weights.value(k, c, kh, kw) * input.value(c, ih + kh, iw + kw);
                }
              }
            }
          }
        }
      }
    }
  }

  return output_tensor;
}

struct rng_t
{
  std::random_device rd;
  std::mt19937 gen;
  std::uniform_real_distribution<float> dis;

  rng_t() : rd(),
            gen(rd()),
            dis(0., 1.)
  { }

  float operator()()
  {
    return dis(gen);
  }
};

void fill_kernel(rng_t& rng,
                 size_t kernels,
                 size_t channels,
                 size_t kernel_height,
                 size_t kernel_width,
                 tensor<float, 4>& weights)
{
  for (size_t k = 0; k < kernels; ++k) {
    for (size_t c = 0; c < channels; ++c) {
      for (size_t kh = 0; kh < kernel_height; ++kh) {
        for (size_t kw = 0; kw < kernel_width; ++kw) {
          *weights.ptr(k, c, kh, kw) = rng();
        }
      }
    }
  }
}

int main()
{
  std::ifstream train_images("train-images-idx3-ubyte", std::ios::binary);

  auto magic_number = read_from_stream<uint32_t>(train_images);
  auto num_images   = read_from_stream<uint32_t>(train_images);
  auto rows         = read_from_stream<uint32_t>(train_images);
  auto columns      = read_from_stream<uint32_t>(train_images);

  std::cout << "Magic number: " << magic_number << std::endl;
  std::cout << "Num images: " << num_images << std::endl;
  std::cout << "Rows: " << rows << std::endl;
  std::cout << "Columns: " << columns << std::endl;

  rng_t rng;

  for (int i = 0; i < num_images; ++i) {
    auto picture = read_picture(train_images, rows, columns);
    dump_letter(std::cout, picture);

    constexpr size_t channels = 1;
    constexpr size_t kernels = 6;
    constexpr size_t kernel_width = 5;
    constexpr size_t kernel_height = 5;
    size_t output_height = rows - kernel_height + 1;
    size_t output_width = columns - kernel_width + 1;
    
    tensor<float, 4> weights{kernels, channels, kernel_height, kernel_height};

    fill_kernel(rng, kernels, channels, kernel_height, kernel_width, weights);
    auto conv1_output = compute_convolution(kernels,
                                            kernel_height,
                                            kernel_width,
                                            channels,
                                            rows,
                                            columns,
                                            output_height,
                                            output_width,
                                            weights,
                                            picture);

  }

  return 0;
}
