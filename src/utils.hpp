#pragma once

template<typename T, size_t s>
struct swap_bytes
{
  T operator()(T val)
  {
    return T{};
  }
};

template<typename T>
struct swap_bytes<T, 4>
{
  T operator()(T val)
  {
    return ((((val) & 0xff000000) >> 24) |
	    (((val) & 0x00ff0000) >>  8) |
	    (((val) & 0x0000ff00) <<  8) |
	    (((val) & 0x000000ff) << 24));
  }
};

template<typename T>
struct swap_bytes<T, 1>
{
  T operator()(T val)
  {
    return val;
  }
};

template<typename T>
T change_endiness(T val)
{
  using bs = swap_bytes<T, sizeof(T)>;

  bs s;
  return s(val);
}

template<typename T>
char* as(T& out)
{
  return reinterpret_cast<char*>(&out);
}

template<typename T>
T read_from_stream(std::ifstream& stream)
{
  T v;
  stream.read(as<T>(v), sizeof(T));
  return change_endiness<T>(v);
}

