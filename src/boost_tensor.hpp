#ifndef _TENSOR_HPP
#define _TENSOR_HPP

#include <boost/fusion/container/vector.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_vector.hpp>
#include <boost/fusion/algorithm/iteration/accumulate.hpp>
#include <boost/fusion/view/iterator_range.hpp>
#include <boost/fusion/include/iterator_range.hpp>
#include <boost/fusion/iterator/next.hpp>
#include <boost/fusion/include/next.hpp>
#include <boost/fusion/include/find.hpp>

struct Dim
{
  size_t value;  

  Dim(size_t _value) : value(_value)
  { }
};

namespace bf = boost::fusion;

template<typename... Dims>
class dims_t
{
  bf::vector<Dims...> dims;

public:
  dims_t(Dims... _dims) : dims(_dims...)
  { }

  size_t size()
  {
    return bf::accumulate(dims, 1, 
             [](auto a, auto b) -> size_t {
               return a * b.value;
             });
  }
  
  template<typename... Offsets>
  size_t offset(Offsets... offsets) const
  {
    auto offs = bf::make_vector(offsets...);
    auto a = [=](auto a, auto b) -> size_t {
      auto f = bf::next(bf::find<decltype(b)>(dims));
      auto e = bf::end(dims);
      typedef typename bf::iterator_range<decltype(f), decltype(e)> rt;
      return a + b.value * bf::accumulate(rt(f, e), 1, 
                                          [](auto a, auto b) -> size_t {
                                            return a * b.value;
                                          });
    };

    return boost::fusion::accumulate(offs, 0, a);
  }

  template<typename Dim>
  size_t dim()
  {
    auto it = bf::find<Dim>(dims);
    return (*it).value;
  }
};

template<typename... Dims>
class tensor_t
{
private:
  dims_t<Dims...> dims;
  float* _ptr;

public:
  typedef float value_type;

  tensor_t(Dims... _dims)
    : dims(_dims...)
  {
    _ptr = new float[dims.size()];
  }

  size_t size()
  {
    return dims.size();
  }

  template<typename... Offsets>
  float value(Offsets... offsets) const
  {
    return _ptr[dims.offset(offsets...)];
  }

  template<typename... Offsets>
  float* ptr(Offsets... offsets)
  {
    return &_ptr[dims.offset(offsets...)];
  }

/*
  template<typename Dim>
  size_t dim()
  {
    return dims.dim<Dim>();
  }
*/

  template<typename Dim>
  size_t dim()
  {
    return 0;
  }

  ~tensor_t()
  {
    delete[] _ptr;
  }
};

#endif
