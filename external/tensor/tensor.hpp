#pragma once

#include <cstddef>
#include <memory>
#include "dims.hpp"

template<typename val_type, size_t dims>
class tensor {
private:
    offsets<dims> _offsets;
    std::unique_ptr<val_type[]> _ptr;

public:
    template<typename... dim_type>
    explicit tensor(dim_type... _dims)
    : _offsets{_dims...}
    , _ptr{new val_type[_offsets.size()]}
    { }

    size_t size() {
        return _offsets.size();
    }

    template<typename... dim_type>
    float value(dim_type... offsets) {
        return _ptr[_offsets.index(offsets...)];
    }

    template<typename... dim_type>
    float* ptr(dim_type... offsets) {
        size_t s = _offsets.index(offsets...);
        return &_ptr[s];
    }

    float* ptr() {
        return _ptr.get();
    }
};
