#pragma once

namespace detail {
    template<size_t idx, size_t dims>
    struct offsets_aux {
        static size_t value() {
            return 1;
        }

        template<typename H>
        static size_t value(H h) {
            return h;
        }

        template<typename H, typename... Ts>
        static size_t value(H h, Ts... ts) {
            if (idx >= dims)
                return h * offsets_aux<idx, dims-1>::value(ts...);
            else
                return offsets_aux<idx, dims-1>::value(ts...);
        }
    };

    template<size_t idx, size_t dims>
    struct calculate_offset {
        template<typename... Ts>
        static size_t value(Ts... offsets) {
            return offsets_aux<idx, dims>::value(offsets...);
        }
    };

    size_t calculate_size() {
        return 1;
    }

    template<typename H>
    size_t calculate_size(H h) {
        return h;
    }

    template<typename H, typename... Ts>
    size_t calculate_size(H h, Ts... ts) {
        return h * calculate_size(ts...);   
    }

    size_t head() {
        return 1;
    }

    template<typename H>
    H head(H h) {
        return h;
    }

    template<typename H, typename... Ts>
    H head(H h, Ts... ts) {
        return h;
    }

    template<size_t n>
    struct offsets;

    template<>
    struct offsets<0> {
        size_t _offset_1 = 0;
        size_t _size = 1;

    };

    template<>
    struct offsets<1> {
    private:
        size_t _offset_1 = 1;
        size_t _size;

    public:
        explicit offsets(size_t dim) {
            _size = dim;
        }

        size_t index(size_t idx_1) {
            return idx_1;
        }

        size_t size() {
            return _size;
        }
    };

    template<size_t dims>
    offsets<0> tail_aux() {
        return offsets<0>{};
    }

    template<size_t dims, typename H>
    offsets<0> tail_aux(H h) {
        return offsets<0>{};
    }

    template<size_t dims, typename H, typename... Ts>
    offsets<dims-1> tail_aux(H h, Ts... ts) {
        return offsets<dims-1>{ts...};
    }

    template<size_t dims, typename... Ts>
    offsets<dims-1> tail(Ts... dimensions) {
        return tail_aux<dims>(dimensions...);
    }

    template<size_t n>
    struct offsets
    {
    private:
        offsets<n-1> _rest;
        size_t _offset_n;
        size_t _size;

        template<typename T, typename... Ts>
        size_t index_aux(T h, Ts... ids) {
            return _rest.index(ids...);
        }

    public:
        template<typename... Ts>
        explicit offsets(Ts... dims)
        : _rest{detail::tail<n>(dims...)}
        , _offset_n{detail::calculate_offset<n-1, n>::value(dims...)}
        , _size{detail::head(dims...)*_rest.size()}
        { }

        template<typename... Ts>
        size_t index(Ts... ids) {
            return detail::head(ids...) * _offset_n + index_aux(ids...);
        }

        size_t size() {
            return _size;
        }

    };
}

template<size_t n>
using offsets = detail::offsets<n>;

